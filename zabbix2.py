from pyzabbix import ZabbixAPI
import yaml
import os
from jinja2 import Environment, FileSystemLoader

# Mengatur Kredensial Zabbix
ZABBIX_SERVER = 'https://monitoring.dwp.net.id'
ZABBIX_USERNAME = 'elga.bayu'
ZABBIX_PASSWORD = '!Hantu115'

# Membuat koneksi ke Zabbix API
zapi = ZabbixAPI(ZABBIX_SERVER)
zapi.login(ZABBIX_USERNAME, ZABBIX_PASSWORD)

# Langkah 1: Mengambil Data Host dari Zabbix
def get_zabbix_hosts():
    try:
        hosts = zapi.host.get(output="extend")
        print("Data host berhasil ditarik dari Zabbix")
        return hosts
    except Exception as e:
        print(f"Error getting host data from Zabbix: {e}")
        return []

# Langkah 2: Menyimpan Data Host ke dalam Folder
def save_hosts_to_folders(hosts):
    template_env = Environment(loader=FileSystemLoader('.'))
    template = template_env.get_template('template1.yml.j2')

    # Buat folder untuk data lengkap
    os.makedirs('data-lengkap', exist_ok=False)

    for host_data in hosts:
        # Cek apakah nama host sesuai dengan kriteria
        if host_data['name'].startswith('[LM]') or host_data['name'].startswith('[Lastmile]'):
            # Menggunakan alamat IP dari kunci 'ip' dalam data host
            ip_address = host_data['host']

            # Melakukan rendering template dengan data host saat ini
            rendered_template = template.render(
                host=host_data['host'],
                name=host_data['name'],
                groups=[{'groupid': ''}],
                templates=[{'templateid': ''}],
                status=host_data['status'],
                hostid=host_data['hostid'],
                tags=[
                    {'tag': 'name', 'value': host_data['name']},
                    {'tag': 'ip', 'value': ip_address}  # Menggunakan alamat IP yang sesuai
                ],
                interfaces=[
                    {
                        'interfaceid': '',
                        'hostid': '',
                        'main': '',
                        'type': '',
                        'useip': '',
                        'ip': ip_address,  # Menggunakan alamat IP yang sesuai
                        'dns': '',
                        'port': '',
                        'details': []
                    }
                ]
            )
            file_name = os.path.join('data-lengkap', f"{host_data['name']}.yaml")  # Nama file dalam folder data-lengkap
            with open(file_name, 'w') as file:
                file.write(rendered_template)  # Menyimpan data ke dalam file YAML
            print(f"Data host {host_data['name']} tersimpan di folder data-lengkap")

# Langkah 3: Main Function
def main():
    hosts = get_zabbix_hosts()
    if hosts:
        save_hosts_to_folders(hosts)

if __name__ == "__main__":
    main()
